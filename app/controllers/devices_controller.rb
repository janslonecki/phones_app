class DevicesController < ApplicationController
before_action :find_device, only: [:destroy]
before_filter :must_be_admin, only: [:edit, :new]
impressionist :actions=>[:show]
require 'will_paginate/array' 

def index
    if params[:search]
      @search = Device.search do
      fulltext params[:search]

      paginate :page => params[:page], :per_page => 40
    end
      @devices = @search.results
    else
    @devices = Device.all.paginate(:page => params[:page], :per_page => 40)
end
    set_meta_tags title: "Search results",
                keywords: "smartphones, mobile, specifications, reviews, camera, specs",
                description: "Home of device specifications and reviews"
end

def home
    @devices = Device.order('impressions_count DESC').first(12)
    set_meta_tags title: "Home of device specifications and reviews",
                keywords: "smartphones, mobile, specifications, reviews, camera, specs",
                description: "Home of device specifications and reviews"
end
 
def new
  @brand = Brand.find(params[:brand_id])
	  @device = Device.new
end

   def create
    @brand = Brand.find(params[:brand_id])
    @device = @brand.devices.create(device_params)
    @device.thumbnail = params[:fog]

    if @device.save
        redirect_to edit_brand_device_path(@brand, @device)
    else
        render 'new'
    end
   end


   def destroy
      @device.destroy
      redirect_to brand_path(@brand)
   end


def adminpanel
  @brands = Brand.all
end

def popular
   @devices = Device.order('impressions_count DESC').paginate(:page => params[:page], :per_page => 40)
end


def show
	@device = Device.find(params[:id])
  @popular = Device.order('impressions_count DESC').first(6)
  @reviews = @device.reviews.all
  if @reviews.present?
  @device.rating = @reviews.sum(:overall) / @reviews.size
  @speed_rating = ((@reviews.sum(:speed) / @reviews.size.to_f) / 5) * 100
  @durability_rating = ((@reviews.sum(:durability) / @reviews.size.to_f) / 5) * 100
  @display_rating = ((@reviews.sum(:display) / @reviews.size.to_f) / 5) * 100
  @camera_rating = ((@reviews.sum(:camera) / @reviews.size.to_f) / 5) * 100
  @price_quality_rating = ((@reviews.sum(:price_quality) / @reviews.size.to_f) / 5) * 100
  end
    @brand = @device.brand
    @brands = Brand.all.where.not(id: @device.brand.id)
    @brandsdevices = @brand.devices.all.where.not(id: @device.id)
    @images = @device.images << @device.thumbnail
    impressionist(@device, nil, :unique => [:session_hash])

    set_meta_tags title: "#{@device.brand.name} #{@device.name} - Device specifications and reviews",
                keywords: "#{@device.brand.name}, #{@device.name}, specs, specifications, reviews, ratings, dimensions, weight, CPU, processor, camera, GPU, Soc, speed, price, ram, battery, display, os",
                description: "#{@device.brand.name} #{@device.name} - Device specifications and reviews. #{@device.brand.name} #{@device.name} #{@device.year} is an #{@device.os} - based device with a #{@device.display} display and a #{@device.cpucores}-cored #{@device.cpu} CPU. It comes with #{@device.storage} of storage and weights #{@device.weight}."
end


def edit
    @device = Device.find(params[:id])
end

def update
    @device = Device.find(params[:id])
    if @device.update(device_params)
       redirect_to device_path(@device)
        else
        render 'new'
    end
end

def compare
    @device = Device.find(params[:device_id])
    @brand = @device.brand
    @device2 = Device.find(params[:dev])
    @brand2 = @device2.brand
    @images = @device.images << @device.thumbnail
    @images2 = @device2.images << @device2.thumbnail
  @reviews = @device.reviews.all
  if @reviews.present?
  @device.rating = ((@reviews.sum(:overall) / @reviews.size.to_f) / 5) * 100
  if @device.rating.nan?
    @device.rating = 0
  end
  @speed_rating = ((@reviews.sum(:speed) / @reviews.size.to_f) / 5) * 100
  @durability_rating = ((@reviews.sum(:durability) / @reviews.size.to_f) / 5) * 100
  @display_rating = ((@reviews.sum(:display) / @reviews.size.to_f) / 5) * 100
  @camera_rating = ((@reviews.sum(:camera) / @reviews.size.to_f) / 5) * 100
  @price_quality_rating = ((@reviews.sum(:price_quality) / @reviews.size.to_f) / 5) * 100
  end

  @reviews2 = @device2.reviews.all
  if @reviews2.present?
  @device2.rating = ((@reviews2.sum(:overall) / @reviews2.size.to_f) / 5) * 100
  if @device2.rating.nan?
    @device2.rating = 0
  end
  @speed_rating2 = ((@reviews2.sum(:speed) / @reviews2.size.to_f) / 5) * 100
  @durability_rating2 = ((@reviews2.sum(:durability) / @reviews2.size.to_f) / 5) * 100
  @display_rating2 = ((@reviews2.sum(:display) / @reviews2.size.to_f) / 5) * 100
  @camera_rating2 = ((@reviews2.sum(:camera) / @reviews2.size.to_f) / 5) * 100
  @price_quality_rating2 = ((@reviews2.sum(:price_quality) / @reviews2.size.to_f) / 5) * 100
  end
 set_meta_tags title: "#{@device.brand.name} #{@device.name} vs #{@device2.brand.name} #{@device2.name} - Devices comparison",
                keywords: "#{@device.brand.name}, #{@device.name}, specs, specifications, reviews, ratings, dimensions, weight, CPU, processor, camera, GPU, Soc, speed, price, ram, battery, display, os",
                description: "#{@device.brand.name} #{@device.name} - Specifications and reviews"
end

    private

        def device_params
            params.require(:device).permit(:name, :brand_id, :dimensions, :weight, :soc, :cpu, :cpucores, :gpu, :ram, :storage, :display, :battery, :os, :camera, :year, :thumbnail, :thumbnailsm, {images:[]})
        end

        def find_device
            @device = Device.find(params[:id])
        end

        def find_brand
        	@brand = Brand.find(params[:brand_id])
        end


        def must_be_admin
        unless current_user && current_user.admin?
          redirect_to root_path
        end
        end

end