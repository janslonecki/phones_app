class ReviewsController < ApplicationController

def create
	@device = Device.find(params[:device_id])
    @review = @device.reviews.create(review_params)
    ratings = [@review.speed, @review.durability, @review.display, @review.camera, @review.price_quality]
    @review.overall = ratings.sum / ratings.size.to_f
    @review.user = current_user
    if @review.save
    	redirect_to device_path(@device)
    else
    	redirect_to device_path(@device)
    end
end

private
  
  def review_params
  	params.require(:review).permit(:content, :speed, :durability, :display, :camera, :price_quality, :user_id, :device_id)
  end

end