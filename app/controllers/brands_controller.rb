class BrandsController < ApplicationController
before_action :find_brand, only: [:destroy]
before_filter :must_be_admin, only: [:edit, :new]
def index
	@brands = Brand.all
    @popular = Device.order('impressions_count DESC').first(6)
end

def home
    @brands = Brand.all
end

def new
	@brand = Brand.new
end

def create
    @brand = Brand.new(brand_params)
    if @brand.save
    	redirect_to edit_brand_path(@brand)
    else
    	render 'new'
    end
end


def show
    @brand = Brand.find(params[:id])
    @popular = Device.order('impressions_count DESC').first(6)
    @brands = Brand.all.where.not(id: @brand.id)
    @devices = @brand.devices.all
    set_meta_tags title: "#{@brand.name} devices - Home of device specifications and reviews",
                keywords: "smartphones, mobile, specifications, reviews, camera",
                description: "Home of device specifications and reviews"
end

def correct
    @brand = Brand.find(params[:brand_id])
    @devices = @brand.devices.all
end

def destroy
	@brand.destroy
    redirect_to root_path
end

def edit
    @brand = Brand.find(params[:id])
    @devices = @brand.devices.all 
end

def update
    @brand = Brand.find(params[:id])
    if @brand.update(brand_params)
       redirect_to edit_brand_path(@brand)
        else
        render 'new'
    end
end

    private

        def find_brand
            @brand = Brand.find(params[:id])
        end

        def brand_params
            params.require(:brand).permit(:name, :brandav, devices_attributes: [:id, :name, :dimensions, :weight, :soc, :cpu, :cpucores, :gpu, :ram, :storage, :display, :battery, :os, :camera, :year, :thumbnail, :thumbnailsm, {images:[]}])
        end

        def device_params
            params.require(:device).permit(:name, :brand_id, :dimensions, :weight, :soc, :cpu, :cpucores, :gpu, :ram, :storage, :display, :battery, :os, :camera, :year, :thumbnail, :thumbnailsm, {images:[]})
        end

        def must_be_admin
        unless current_user && current_user.admin?
          redirect_to root_path
        end
        end
end