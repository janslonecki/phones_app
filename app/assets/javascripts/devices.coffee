# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
		jQuery -> 
		    window.devid = $('#comparewrap').attr('data')
		    devices = $('#_devices_'+window.devid+'_compare_device').html()
		    brand = $('#_devices_'+window.devid+'_compare_brand :selected').text()
		    options = $(devices).filter("optgroup[label='#{brand}']").html()
		    if options
		    	$('#_devices_'+window.devid+'_compare_device').html(options)
		    else
		    	$('#_devices_'+window.devid+'_compare_device').empty()
		    $('#_devices_'+window.devid+'_compare_brand').change ->
			    brand = $('#_devices_'+window.devid+'_compare_brand :selected').text()
			    options = $(devices).filter("optgroup[label='#{brand}']").html()
			    if options
			    	$('#_devices_'+window.devid+'_compare_device').html(options)
			    else
			    	$('#_devices_'+window.devid+'_compare_device').empty()

			$('#_devices_'+window.devid+'_compare_device').change ->
	            compareid = $('#_devices_'+window.devid+'_compare_device option:selected').attr('value')
	            $('#comparelink').attr('href', '/devices/'+devid+'/compare?dev='+compareid)

		    $('#_devices_'+window.devid+'_compare_brand').change ->
	            compareid = $('#_devices_'+window.devid+'_compare_device option:selected').attr('value')
	            $('#comparelink').attr('href', '/devices/'+devid+'/compare?dev='+compareid)
