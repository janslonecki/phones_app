module ApplicationHelper

def default_meta_tags
  {
    title: "Home of device specifications and reviews",
    keywords: "smartphones, mobile, specifications, reviews, camera",
    description: "Home of device specifications and reviews"
  }
end

end
