class Device < ActiveRecord::Base
  belongs_to :brand
  has_many :reviews
  is_impressionable :column_name => :impressions_count, :unique => :session_hash, :counter_cache => true
  acts_as_votable

  searchable do
	text :name
	text :brand_name do
		brand.name
	end
  end

  mount_uploader :thumbnail, ThumbnailUploader
  mount_uploader :thumbnailsm, ThumbnailsmUploader
  mount_uploaders :images, ImagesUploader
end
