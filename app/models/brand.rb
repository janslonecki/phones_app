class Brand < ActiveRecord::Base
has_many :devices, dependent: :destroy
acts_as_votable


mount_uploader :brandav, BrandavUploader
accepts_nested_attributes_for :devices

end
