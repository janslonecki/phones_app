class AddRatingToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :rating, :float,  :default => 0
  end
end
