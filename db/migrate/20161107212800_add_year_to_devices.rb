class AddYearToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :year, :integer
  end
end
