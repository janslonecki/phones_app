class AddDeviceToReviews < ActiveRecord::Migration
  def change
    add_reference :reviews, :device, index: true, foreign_key: true
  end
end
