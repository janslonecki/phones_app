class AddThumbnailToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :thumbnail, :string
  end
end
