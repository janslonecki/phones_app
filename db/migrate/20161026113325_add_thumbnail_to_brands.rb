class AddThumbnailToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :thumbnail, :string
  end
end
