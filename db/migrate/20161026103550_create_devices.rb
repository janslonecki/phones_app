class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :name
      t.string :dimensions
      t.string :weight
      t.string :soc
      t.string :cpu
      t.string :gpu
      t.string :ram
      t.string :storage
      t.string :display
      t.string :battery
      t.string :os
      t.string :camera
      t.string :cpucores
      t.references :brand, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
