class AddCounterCacheToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :impressions_count, :integer, default: 0
  end
end
