class RemoveThumbnailFromBrands < ActiveRecord::Migration
  def change
    remove_column :brands, :thumbnail, :string
  end
end
