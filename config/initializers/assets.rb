# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

Rails.application.config.assets.precompile += %w( foundation_and_overrides.scss )

Rails.application.config.assets.precompile += %w( custom.scss.erb )

Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile += %w( comparestyle.scss.erb )

Rails.application.config.assets.precompile += %w( swipebox.css )

Rails.application.config.assets.precompile += %w( jquery.swipebox.js )

Rails.application.config.assets.precompile += %w( icons.png )

Rails.application.config.assets.precompile += %w( icons.svg )

Rails.application.config.assets.precompile += %w( loader.gif )

Rails.application.config.assets.precompile += %w( bg.jpg )

Rails.application.config.assets.precompile += %w( bgsm.png )

Rails.application.config.assets.precompile += %w( dimensions.png )

Rails.application.config.assets.precompile += %w( weight.png )

Rails.application.config.assets.precompile += %w( soc.png )

Rails.application.config.assets.precompile += %w( cpu.png )

Rails.application.config.assets.precompile += %w( cpucores.png )

Rails.application.config.assets.precompile += %w( gpu.png )

Rails.application.config.assets.precompile += %w( ram.png )

Rails.application.config.assets.precompile += %w( storage.png )

Rails.application.config.assets.precompile += %w( display.png )

Rails.application.config.assets.precompile += %w( battery.png )

Rails.application.config.assets.precompile += %w( os.png )

Rails.application.config.assets.precompile += %w( camera.png )


# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
