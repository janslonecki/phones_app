Rails.application.routes.draw do

  resources :articles
  # The priority is based upon order of creation: first created -> highest priority.
root 'devices#home'

resources :brands do
get '/correct', to: 'brands#correct'
get '/count', to: 'brands#count_scrapes'
resources :devices
end

resources :devices do
resources :reviews
get '/compare', to: 'devices#compare'
end

get '/adminpanel', to: 'devices#adminpanel'

get '/popular', to: 'devices#popular'

devise_for :users, :controllers => { :confirmations => 'confirmations', :omniauth_callbacks => 'users/omniauth_callbacks' }
end
